//
//  CMTime + Extension.swift
//  iMusic
//
//  Created by Vlad Zaporozhskyi on 2020-02-18.
//  Copyright © 2020 Vlad Zaporozhskyi. All rights reserved.
//

import AVKit

extension CMTime {
    func displayString() -> String {
        guard !CMTimeGetSeconds(self).isNaN else { return "" }
        let totalSeconds = Int(CMTimeGetSeconds(self))
        let seconds = totalSeconds % 60
        let minutes = totalSeconds / 60
        let timeFormattedString = String(format: "%02d:%02d", minutes, seconds)
        return timeFormattedString
    }
}
