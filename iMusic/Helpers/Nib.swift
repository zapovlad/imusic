//
//  Nib.swift
//  iMusic
//
//  Created by Vlad Zaporozhskyi on 2020-02-25.
//  Copyright © 2020 Vlad Zaporozhskyi. All rights reserved.
//

import UIKit

extension UIView {
    class func loadFromNib<T: UIView>() -> T {
        return Bundle.main.loadNibNamed(String(describing: T.self), owner: nil, options: nil)![0] as! T
    }
}
